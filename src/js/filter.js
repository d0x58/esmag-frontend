$(document).ready(function() {
    /* Groups toggle */
    $('body').on('click', '.cf__title.cf__title--toggled', function() {
        $(this).toggleClass('is-open');
    });

    /* Range Slider */
    $('.cf__range-slider').each(function(){

        var $t = $(this),
            t = this;

        var min = $(this).data('min') || 0,
            max = $(this).data('max') || 100,
            start = $(this).data('start') || min,
            end = $(this).data('end') || max;

        var $prnt = $(this).closest('.cf__range-group'),
            $inputs = $prnt.find('input');
            inputStart = $inputs.get(0),
            inputEnd = $inputs.get(1);


        $(this).data('inputStart', inputStart);
        $(this).data('inputEnd', inputEnd);



        noUiSlider.create(this, {
            start: [ start, end ],
            connect: true,
            range: {
                'min': min,
                'max': max
            }
        });

        this.noUiSlider.on('update', function( values, handle ) {

            var value = values[handle];

            if ( !handle ) {
                $t.data('inputStart').value = value;
            } else {
                $t.data('inputEnd').value = value;
            }
        });


        inputStart.addEventListener('change', function(){
            t.noUiSlider.set([this.value, null]);
        });
        inputEnd.addEventListener('change', function(){
            t.noUiSlider.set([null, this.value]);
        });


    });



});
