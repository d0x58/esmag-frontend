$(document).ready(function() {

    $body = $('body');

    // Navigation toggle (mobile)
    var $headerNavCont = $('#header-nav-cont');
    $('.js-navbar-toggle').click(function(){

        $headerNavCont.toggleClass('is-active');
    });


    // Catalog top carousel
    var $catalogTopSlider = $('#catalog-top-slider'),
        ctsDragging = false;

    $catalogTopSlider.owlCarousel({
        navigation: true,
        loop: false,
        navigationText: [
            "<i class='cm-slider__nav cm-slider__nav--prev'></i>",
            "<i class='cm-slider__nav cm-slider__nav--next'></i>",
        ],
        items : 16, //10 items above 1000px browser width
        itemsDesktop : [1000,10], //5 items between 1000px and 901px
        itemsDesktopSmall : [900,10], // betweem 900px and 601px
        itemsTablet: [600,7], //2 items between 600 and 0
        itemsMobile : false, // itemsMobile disabled - inherit from itemsTablet option
        startDragging: function(){
            ctsDragging=true;
            //console.log('startDragging');
        },
        afterMove: function(){
            ctsDragging=false;
            //console.log('afterMove');
        }
    });


    // Catalog top menu
        var $cmSlideSubMenu = $('.cm-slider__sub-menu'),
        $cmSliderItem = $('.cm-slider__item'),
        $cmSlideGroup = $('.cm-slider__group'),
        $cmCatalogNav = $('.cm-catalog-nav'),
        cmIsOpen = false;

    // $('.uk-notouch .js-cm-slider').click(function(e){
    //     openCatTopnav(e);

    // });
    // $('.uk-touch .js-cm-slider').on('touchend', function(e){
    //     openCatTopnav(e);
    // });

    $('.cm-slider__item').click(function(e){
        $(event.currentTarget).addClass('is-open-bord');
        openCatTopnav(e);

    });
    $('.cm-slider__item').on('touchend', function(e){
        $(event.currentTarget).addClass('is-open-bord');
        openCatTopnav(e);
    });



    function openCatTopnav(e) {

        if ($(e.target).hasClass('cm-slider__nav')) return false;
        if (ctsDragging) {
            ctsDragging = false;
            return false;
        }


        $cmSlideSubMenu.toggleClass('is-open');
        cmIsOpen = $cmSlideSubMenu.hasClass('is-open');
        $('.cm-slider__item-name').css('visibility', 'hidden');
        if (cmIsOpen == false) {
            $('.cm-slider__item-name').css('visibility', 'visible');
            $('.cm-slider__item').removeClass('is-open-bord');
        }

    }


    $('.cm-slider__item').mouseenter(function () {

        if (cmIsOpen !== false) {
            $('.cm-slider__item').removeClass('is-open-bord');
            $(event.currentTarget).addClass('is-open-bord');
        }

        })

   $cmSliderItem.on('mouseenter', function(){
        var indx = $cmSliderItem.index($(this));
        $cmSlideGroup.hide().eq(indx).show();

    });


    $cmCatalogNav.on('mouseleave', function(){
        $cmSlideSubMenu.removeClass('is-open');
        cmIsOpen = false;
        $('.cm-slider__item-name').css('visibility', 'visible');
        $('.cm-slider__item').removeClass('is-open-bord');
    });


    // Main slider
    var $mainSlider = $('#m-slider');

    $mainSlider.owlCarousel({
        items : 1,
        itemsDesktop : [1000,1], //5 items between 1000px and 901px
        itemsDesktopSmall : [900,1], // betweem 900px and 601px
        itemsTablet: [600,1], //2 items between 600 and 0
        itemsMobile : false, // itemsMobile disabled - inherit from itemsTablet option
        autoPlay: 5000,
        afterMove: function(){
            mainSliderGoTo(this.owl.currentItem);
        },
    });
    var mainSliderOwl = $("#m-slider").data('owlCarousel'),
        $mSliderControlItem = $('.m-slider__control-item');

    $mSliderControlItem.click(function(){
        var index = $mSliderControlItem.index($(this));

        mainSliderGoTo(index);
    });

    function mainSliderGoTo(index) {
        mainSliderOwl.goTo(index);
        $mSliderControlItem.removeClass('is-active');
        $mSliderControlItem.eq(index).addClass('is-active');
    }

    // Main slider scroll pane
    // Инициализирум только после загрузки всех изображений
    var $mainSliderScrollPane = $('#main-slider-scroll-pane'),
        $mSlideItem = $('.m-slider__item img'),
        loadedImage = 0;

    $mSlideItem.one("load", function() {
        loadedImage++;
        if (loadedImage == $mSlideItem.length) {
            $mainSliderScrollPane.customScrollbar({
                skin: 'default-skin'
            });
        }
    }).each(function() {
        if(this.complete) $(this).load();
    });


    // Fixed cart scroll pane
    var $fixedCContent = $('.fixed__c-content');
    $fixedCContent.customScrollbar({
        skin: 'default-skin white-skin'
    });





    // Brands slider
    var $brandsSlider = $('#b-slider');
    $brandsSlider.owlCarousel({
        items : 8,
        itemsDesktop : [1000,8], //5 items between 1000px and 901px
        itemsDesktopSmall : [900,6], // betweem 900px and 601px
        itemsTablet: [600,2], //2 items between 600 and 0
        itemsMobile : false, // itemsMobile disabled - inherit from itemsTablet option
        autoPlay: 2500,
        navigation : true,
        navigationText : [
            "<div class='main-brands__nav main-brands__nav--next'></div>",
            "<div class='main-brands__nav main-brands__nav--prev'></div>",
        ],
        pagination : true,
        paginationNumbers: false,
    });


    // Brands filter
    $('#container').mixItUp({
        animation: {
            duration: 350,
        },
    });
    var $brandsChar = $('.brands__char-item');
    $brandsChar.click(function(){
        $brandsChar.removeClass('is-active');
        $(this).addClass('is-active');
    });


    // Delivery datatable
    $('.deliv-city-table').DataTable({
        "language": {
            "lengthMenu": "Записей _MENU_ на странице",
            "zeroRecords": "Ничего не найдено",
            "info": "Страница _PAGE_ из _PAGES_",
            "infoEmpty": "Нет записей",
            "infoFiltered": "",
            "paginate": {
                "previous": "←",
                "next": "→"
            },
            "search": "Поиск",
            "display": "Количество",
        },
        "dom": '<"top"lf>rt<"bottom"pi><"clear">'

    } );


    /* Input Mask */
    $('.input-mask--phone').mask("+7 (999) 999-99-99");



    /* View history slider */
    var $vhSlider = $('#vh-slider');

    $vhSlider.owlCarousel({
        items : 4,
        itemsDesktop : [1000,4], //5 items between 1000px and 901px
        itemsDesktopSmall : [900,2], // betweem 900px and 601px
        itemsTablet: [600,1], //2 items between 600 and 0
        itemsMobile : false, // itemsMobile disabled - inherit from itemsTablet option
        autoPlay: 5000,
        navigation : true,
        navigationText : [
            "<div class='main-brands__nav main-brands__nav--with-paddings main-brands__nav--next'></div>",
            "<div class='main-brands__nav main-brands__nav--with-paddings main-brands__nav--prev'></div>",
        ],
    });

    /* Popular goods slider */
    var $pgSlider = $('#pg-slider');

    $pgSlider.owlCarousel({
        items : 4,
        itemsDesktop : [1000,4], //5 items between 1000px and 901px
        itemsDesktopSmall : [900,2], // betweem 900px and 601px
        itemsTablet: [600,1], //2 items between 600 and 0
        itemsMobile : false, // itemsMobile disabled - inherit from itemsTablet option
        autoPlay: 5000,
        navigation : true,
        navigationText : [
            "<div class='main-brands__nav main-brands__nav--with-paddings main-brands__nav--next'></div>",
            "<div class='main-brands__nav main-brands__nav--with-paddings main-brands__nav--prev'></div>",
        ],
    });


    /* Catalog grid view toggle */
    var $cgvControls = $('.js-catalog-to-th, .js-catalog-to-th-list'),
        $catGrid = $('.cg__grid');
    $body.on('click', '.js-catalog-to-th, .js-catalog-to-th-list', function(){

        $cgvControls.removeClass('is-active');
        $(this).addClass('is-active');

        if ($(this).hasClass('js-catalog-to-th')) {
            $catGrid.addClass('cg__cells');
        } else {
            $catGrid.removeClass('cg__cells');
        }
    });



    /* catalog good show more */
    $body.on('click', '.js-good-about-more', function(){
        $(this).closest('.cgood__about-cont').toggleClass('is-open');
    });

});

